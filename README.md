 

# EnOcean Aruba IoT Demo

## Demo Description

The [EnOcean](https://www.enocean.com/en/) [Aruba IoT Demo](https://www.enocean.com/en/applications/iot-solutions/) provides an easy way to visualize the data sent by EnOcean Wireless devices to Aruba access point. The demo creates a local web socket to receive the data from the Aruba access points and visualizes the data in a simple way. It is intended to show the operation of the EnOcean to Aruba connection and display the data in a simple format. For more advanced operation and cloud based software applications please reach out to us at [iot@enocean.com](mailto:iot@enocean.com).  

The following EEPs ([EnOcean Equipment Profile](https://www.enocean-alliance.org/eep/)) are supported:

* [F6-03-02](http://tools.enocean-alliance.org/EEPViewer/profiles/F6/03/02/F6-03-02.pdf)
* [D5-00-01](http://tools.enocean-alliance.org/EEPViewer/profiles/D5/00/01/D5-00-01.pdf)
* [A5-04-03](http://tools.enocean-alliance.org/EEPViewer/profiles/A5/04/03/A5-04-03.pdf)
* [D2-14-41](http://tools.enocean-alliance.org/EEPViewer/profiles/D2/14/41/D2-14-41.pdf)

These steps will only need to be completed once per computer you wish to install it on. 

## You will need…

* Aruba Access Point with USB port and running Aruba Instant Version  **`8.7.0.0`** or newer (most likely requires firmware update). 
  * [EnOcean IoT Starter Kit](https://www.enocean.com/en/products/enocean_modules/eiska/) (EISK [A](https://www.enocean.com/en/products/enocean_modules/eiska/)/[U](https://www.enocean.com/en/products/enocean_modules_902mhz/eisku/)/J) [USB300A](https://www.enocean.com/en/products/enocean_modules/usb-300/) / [USB500U](https://www.enocean.com/en/products/enocean_modules_902mhz/usb-500u/) / [USB410J](https://www.enocean.com/en/products/enocean_modules_928mhz/usb-400j/)

  * At least one from the following, matching the USB gateway radio frequency (A/U/J):

    * [PTM](https://www.enocean.com/en/products/enocean_modules/ptm-210ptm-215/) based switch

    * [EMSI](https://www.enocean.com/en/products/multisensor/) 5 type Multisensor
    * [ETHS](https://www.enocean.com/en/products/enocean_modules/temperature-humidity-sensor-ethsa-oem/) temp and humidity sensor
    * [EMCS](https://www.enocean.com/en/products/enocean_modules/magnet-contact-sensor-emcsa-oem/) door and window magnet contact. 

Hint: The letters A/U/J represents the frequency so all EnOcean pieces must have a matching designator. For instance, U devices will not work with A or J USB stick. 

> ⚠️⚠️⚠️**ATTENTION**:  Please note that the Aruba AP `Check Version` feature will most likely **NOT** show any available updates. To download the latest Aruba FW version (**`8.7.0.0`** or newer), go to: [Aruba Support Portal](https://asp.arubanetworks.com/) & perform a manual upgrade.
>
> ![](img/checkVersion.png)

## Configuring the Demo

> ⚠️**ATTENTION**: Please make sure to update your Aruba Access Point to SW version **`8.7.0.0`** or newer!!

This tutorial assumes that the Aruba AP has already been already configured, the IP-Address of the AP is known and the computer & AP are in the same subnetwork and/or routing has been properly configured. You will need your Aruba Admin login credentials to proceed. 

**CRITICAL NOTE:** Some Aruba AP’s need a specifically higher power supply in order to use the built in USB port. Based on our experience some Aruba AP’s require 60 Watts of power. This is well specified in the Aruba documentation for your particular Aruba AP.  Please consult the documentation before attempting to use the EnOcean USB300/500 with your Aruba Access Point to validate you have appropriate power supply connected or your USB port will not function.

Please check the power requirements for Aruba AP's in order to properly use the USB port.  

![](img/power_requirement.png)

If you have any questions please contact Aruba Support:

<https://www.arubanetworks.com/support-services/contact-support/>



#### Step 1 Visit [www.enocean.com/en/aruba](http://www.enocean.com/en/aruba) to download the demo 

Extract the downloaded zip file into a directory of your choice.

#### Step 2

Double click on RunEnOceanArubaDemo.bat to start the demo. This will cause two windows to open. 

The first window is a command line instance running the local server, which processes the EnOcean data forwarded by the Aruba AP. There are no steps to take in this window but it does need to remain open in the background for the demo to operate correctly. 

![](img/server_demo.png)

The second window will be The EnOcean IoT demo (client) which visualizes the data received & forwarded by the access point.

  ![](img/dashboard_demo.png)

#### Step 3

The first time the IoT Demo is started the Windows firewall will need to be configured to allow for the IoT demo to establish a connection. 

Configure the firewall as shown below, then click `allow access`.

![](img/firewall_demo.png)

Allow Domain Networks & Private Networks

#### Step 4

Go to the IoT Demo (the part of this demo being displayed on the web browser):

 ![](img/dashboard_disc_demo.png)

Click on the black bar showing the end-point link. This will copy the IoT Demo address to the clipboard. A confirmation pop-up will be shown. 

![](img/endpoint_demo.png)

#### Step 5

In a separate tab navigate to your Aruba AP's IP-address, select the administrator interface. Log in using your Aruba credentials. 

Navigate to `Configuration` &rarr; `Services`, Then select `IoT`.

  ![](img/iot_profile_demo.png)

#### Step 6

* On IoT-Transport Streams section, click on the  **+**. 

  * `Name` field, write a descriptive name for the profile. E.g. *demo*.

  * `Server URL` field, paste the end-point link copied from the dashboard.

  * `Server Type` drop-down select `Telemetry Websocket`.
  * `Device classes` section, select `Serial Data`.
  * Turn the `State` switch `on`.
  * Change the `Reporting interval` to `15`.
  * Go to the `Authentication` section and select `Use token`. 
  * On the `Access Token’`field type: `1234567890`.

* Press **Ok** & then **Save**.

 ![](img/stream_1_demo.png)

![](img/stream_2_demo.png)

#### Step 7

> ⚠️**ATTENTION:** EnOcean solar-powered sensors might require a couple minutes of light after being taken out of the box to start operating. 

Trigger an EnOcean telegram by either pressing any button on the switch rapidly three-times or the teach-in button in one of the sensors. 

Return to the IoT demo interface & wait for the status indicators `Aruba Access-Point status` & `EnOcean traffic` to turn from yellow to green. This indicates the Aruba AP has opened a connection to the local server & EnOcean traffic is flowing in. Three green boxes indicate success!

 ![](img/connected_demo.png)

A pop-up will also be shown in the IoT demo dashboard indicating the IP-address of the Aruba AP connected to the local server.

![](img/notification_demo.png)

> ⚠️⚠️⚠️**ATTENTION:** Should the `EnOcean traffic` indicator remain yellow, this most likely indicates the Aruba AP is **NOT** running a supported firmware version (**`8.7.0.0`** or newer). Updating the AP to the latest version should fix this issue.

## Done!

The EnOcean Aruba IoT Demo has been successfully connected to your access point. 

To begin to visualize the EnOcean sensor data please follow the instructions in RED listed in the IoT demo screen under each device type.  

For support, please open an issue directly on this repo or contact [support@enocean.com](mailto:support@enocean.com) should you experience any difficulties.

When you are ready to expand from a demo to a full solution or if you should have questions about your unique application for EnOcean technology please reach out to [iot@enocean.com](mailto:iot@enocean.com)



#### For advanced users: Binding the Demo to a different network interface

To bind the demo to a different network interface, place a `config.json` file in the same directory as the demo executable with the following contents:

```json
{
    "ip":"192.168.1.92"
}
```

Substitute `192.168.1.92` with the IP-Address assigned to the desired network interface. 



**Disclaimer**

The information provided in this document describes typical features of the EnOcean radio system and should not be misunderstood as specified operating characteristics. No liability is assumed for errors and / or omissions. We reserve the right to make changes without prior notice. For the latest documentation visit the EnOcean website at www.enocean.com